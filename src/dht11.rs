use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use rppal::gpio::{IoPin, Mode};
use serde::Serialize;

use crate::parse::{parse_bytes, Error};

/// Results of a reading performed by the DHT11.
#[derive(Copy, Clone, Default, Debug, Serialize)]
pub struct Measurement {
    /// The measured temperature in tenths of degrees Celsius.
    pub temperature: f64,
    /// The measured humidity in tenths of a percent.
    pub humidity: f64,
}

/// A DHT11 device.
pub struct Dht11 {
    /// The concrete GPIO pin implementation.
    gpio: IoPin,
}

impl Dht11 {
    /// Creates a new DHT11 device connected to the specified pin.
    pub fn new(gpio: IoPin) -> Self {
        Dht11 { gpio }
    }

    /// Performs a reading of the sensor.
    pub fn perform_measurement<D>(&mut self, delay: &mut D) -> Result<Measurement, Error>
    where
        D: DelayUs<u16> + DelayMs<u16>,
    {
        let max_unchanged_count = 10000;

        let mut last = false;
        let mut unchanged_count = 0;

        let mut data = Vec::new();
        data.reserve(40 * 500);

        // Perform initial handshake
        self.perform_handshake(delay)?;

        // Read the response
        let mut current;
        loop {
            current = self.gpio.is_high();
            data.push(current);
            if last != current {
                unchanged_count = 0;
                last = current;
                continue;
            }
            unchanged_count += 1;
            if unchanged_count >= max_unchanged_count {
                break;
            }
        }

        // Parse the response
        let data = parse_bytes(data.iter().map(|&d| d as i32).collect())?;

        // Check CRC
        let crc = data[0]
            .wrapping_add(data[1])
            .wrapping_add(data[2])
            .wrapping_add(data[3]);
        if crc != data[4] {
            return Err(Error::CrcMismatch);
        }

        // Compute temperature
        let mut temp = i16::from(data[2] & 0x7f) * 10 + i16::from(data[3]);
        if data[2] & 0x80 != 0 {
            temp = -temp;
        }

        Ok(Measurement {
            temperature: (temp as f64) / 10f64,
            humidity: (u16::from(data[0]) * 10 + u16::from(data[1])) as f64 / 10f64,
        })
    }

    fn perform_handshake<D>(&mut self, delay: &mut D) -> Result<(), Error>
    where
        D: DelayUs<u16> + DelayMs<u16>,
    {
        self.gpio.set_mode(Mode::Output);
        // Set pin as floating to let pull-up raise the line and start the reading process.
        self.gpio.set_high();
        delay.delay_ms(500);

        // Pull line low for at least 18ms to send a start command.
        self.gpio.set_low();
        delay.delay_ms(20);

        // Restore floating
        self.gpio.set_high();

        self.gpio.set_mode(Mode::Input);

        Ok(())
    }
}
