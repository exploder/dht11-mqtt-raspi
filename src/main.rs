use std::{error, process::exit, thread, time};

use rppal::{
    gpio::{Gpio, Mode, Pin},
    hal::Delay,
};

use rumqttc::{Client, MqttOptions, QoS};
use serde::Deserialize;
use serde_json;

mod dht11;
mod parse;

#[derive(Deserialize, Debug)]
struct Config {
    pin: u8,
    mqtt_host: String,
    mqtt_port: u16,
}

fn read_config() -> Result<Config, Box<dyn error::Error>> {
    let file_content = std::fs::read_to_string("config.toml")?;
    Ok(toml::from_str(&file_content)?)
}

fn get_pin(pin: u8) -> Result<Pin, Box<dyn error::Error>> {
    let gpio = Gpio::new()?;
    Ok(gpio.get(pin)?)
}

fn main() {
    let config: Config = match read_config() {
        Ok(config) => config,
        Err(error) => {
            println!("Error while reading config.toml: {:?}", error);
            exit(1)
        }
    };

    let mut pin = match get_pin(config.pin) {
        Ok(pin) => pin.into_io(Mode::Output),
        Err(err) => {
            println!("Error while opening the pin: {:?}", err);
            exit(1);
        }
    };
    pin.set_high();
    // Create an instance of the DHT11 device
    let mut dht11 = dht11::Dht11::new(pin);
    let mut delay = Delay::new();

    let mut mqttoptions = MqttOptions::new("dht11", config.mqtt_host, config.mqtt_port);
    mqttoptions.set_keep_alive(time::Duration::from_secs(5));
    let (mut client, mut connection) = Client::new(mqttoptions, 10);

    thread::spawn(move || {
        loop {
            // First, do a dummy measurement
            for _ in 0..5 {
                match dht11.perform_measurement(&mut delay) {
                    Ok(_meas) => break,
                    Err(_e) => {}
                }
                thread::sleep(time::Duration::from_secs(1));
            }

            // Then do it proper
            let mut measurement = None;
            for _ in 0..5 {
                match dht11.perform_measurement(&mut delay) {
                    Ok(meas) => measurement = Some(meas),
                    Err(_e) => {}
                }
                thread::sleep(time::Duration::from_secs(1));
            }

            match measurement {
                Some(meas) => client
                    .publish(
                        "dht11",
                        QoS::AtLeastOnce,
                        false,
                        serde_json::to_string(&meas).unwrap(),
                    )
                    .unwrap(),
                None => continue,
            }
            thread::sleep(time::Duration::from_secs(51));
        }
    });

    // Iterate to poll the eventloop for connection progress
    for notification in connection.iter() {
        match notification {
            Ok(_event) => (),
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}
