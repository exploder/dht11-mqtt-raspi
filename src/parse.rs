/// Error type for this crate.
#[derive(Debug)]
pub enum Error {
    /// CRC mismatch.
    CrcMismatch,
    WrongLen(usize),
}

enum ParseState {
    InitPullDown,
    InitPullUp,
    DataFirstPullDown,
    DataPullUp,
    DataPullDown,
}

pub(crate) fn parse_bytes(data: Vec<i32>) -> Result<[u8; 5], Error> {
    let mut lengths = Vec::new();
    let mut prev_idx = 0;
    let mut state = ParseState::InitPullDown;

    for (idx, bit) in data.iter().enumerate() {
        match state {
            ParseState::InitPullDown => match bit {
                0 => state = ParseState::InitPullUp,
                _ => continue,
            },
            ParseState::InitPullUp => match bit {
                1 => state = ParseState::DataFirstPullDown,
                _ => continue,
            },
            ParseState::DataFirstPullDown => match bit {
                0 => state = ParseState::DataPullUp,
                _ => continue,
            },
            ParseState::DataPullUp => match bit {
                1 => {
                    prev_idx = idx;
                    state = ParseState::DataPullDown
                }
                _ => continue,
            },
            ParseState::DataPullDown => match bit {
                0 => {
                    lengths.push(idx - prev_idx);
                    state = ParseState::DataPullUp;
                }
                _ => continue,
            },
        }
    }
    if lengths.len() != 40 {
        return Err(Error::WrongLen(lengths.len()));
    }

    let max = lengths.iter().max().unwrap();
    let min = lengths.iter().min().unwrap();
    let half = (max + min) / 2;

    let bits: Vec<u8> = lengths.iter().map(|&val| (val > half) as u8).collect();

    let mut data = [0; 5];

    for i in 0..40 {
        data[i / 8] <<= 1;
        data[i / 8] |= bits[i];
    }

    Ok(data)
}
