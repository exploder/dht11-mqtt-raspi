FROM rust:1.65 AS builder

RUN rustup target add aarch64-unknown-linux-gnu && \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y gcc-aarch64-linux-gnu

WORKDIR /build
COPY ./src/ /build/src/
COPY ./.cargo/ /build/.cargo/
COPY Cargo.toml Cargo.lock /build/

RUN cargo build --verbose --locked --release

FROM --platform=linux/arm64/v8 debian:bookworm-slim
WORKDIR /app
COPY --from=builder /build/target/aarch64-unknown-linux-gnu/release/dht11-mqtt-raspi ./
CMD [ "./dht11-mqtt-raspi" ]
